#!/usr/bin/env perl
#    formulaOnePipeline.pl: trains on some years of data, predicts a year,
#    leverages SVMlight.
#    Copyright (C) 2015 Andrew Schaumberg schaumberg.andrew@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use diagnostics;
use strict;
use warnings;

# Parse args
my $infile = $ARGV[0] or die "First argument must be an input file, e.g. download.20150301t093810.out";
my @inyears = split(/,/, $ARGV[1]) or die "Second argument must be a comma-delimited list of years to train on";
my $outyear = $ARGV[2] or die "Third argument must be a year to predict";
my $extra_svm_learn_args = $ARGV[3] || "";

my $just_stats = 0; # 0 => normal.  1 => did classification etc, just do stats.

# Important variable initialization
my $all_years = sprintf("%s-%s", join('-', @inyears), $outyear);
my $download_subset_file = sprintf("formula1.%s.out", $all_years);
my $training_file   = sprintf("formula1.%s.%s.format", $all_years, join('-', @inyears));
my $validation_file = sprintf("formula1.%s.%s.format", $all_years, $outyear);

goto stats if $just_stats;


##########################################################################
# Step 1: subsample the input file to have training and validation years #
##########################################################################

open(my $infh, '<', $infile) or die "Cannot open file $infile: $!";
open(my $dlofh, '>', $download_subset_file) or die "Cannot open file $download_subset_file: $!";

my $inyears_alt = join('|', @inyears);
#print "^(#|$inyears_alt|$outyear)\n";
my $inyears_filter = qr/^(#|$inyears_alt|$outyear)/;
while (<$infh>) {
  #print $_;
  if ($_ =~ $inyears_filter) {
    print $dlofh ($_);
  }
}
close($infh);
close($dlofh);


#################################################################################
# Step 2: format data into separate but consistent training and validation sets #
#################################################################################

open(my $fmth, "formulaOneFormat.pl < $download_subset_file |") or die "Cannot exec formulaOneFormat.pl: $!";
open(my $tfh, '>', $training_file)   or die "Cannot open file $training_file: $!";
open(my $vfh, '>', $validation_file) or die "Cannot open file $validation_file: $!";

my $training_filter_str = "^(?:#|[0-9]+ 1:[";
my $relative_year_id = 0;
while ($relative_year_id <= $#inyears) {
  # XXX: assumes single digit relative year ids only
  $training_filter_str = sprintf("%s%d", $training_filter_str, $relative_year_id);
  $relative_year_id += 1;
}
$training_filter_str = sprintf("%s] )", $training_filter_str);
my $training_filter = qr/$training_filter_str/;
my $validation_filter = qr/^(?:#|[0-9]+ 1:$relative_year_id )/;

while(<$fmth>) {
  print $tfh ($_) if ($_ =~ $training_filter);
  print $vfh ($_) if ($_ =~ $validation_filter);
}
close($tfh);
close($vfh);


#############################################
# Step 3: learn classifier on training data #
#############################################

my $model_file = $training_file;
$model_file =~ s/format$/mdl/;
sub runCmd ($) {
  my $cmd = $_[0];
  print("Running: $cmd\n");
  system($cmd);
}
runCmd("svm_learn -z p $extra_svm_learn_args $training_file $model_file");


##################################################################################
# Step 4: make predictions from learned classifier for each race validation data #
##################################################################################

sub hyperplane_val_to_rank ($$) {
  my ($rank_actuals_ref, $prediction_value_on_hyperplane) = @_;
  my $rank_actuals_len = scalar(@{$rank_actuals_ref});
  for(my $i = 0; $i < $rank_actuals_len; $i++) {
    return $i if ($rank_actuals_ref->[$i] == $prediction_value_on_hyperplane);
  }
  die "Cannot find rank for $prediction_value_on_hyperplane";
}

stats: open($vfh, '<', $validation_file) or die "Cannot open file $validation_file: $!";
my %races = (); # race id => lines for race
my $vheader = <$vfh>;
while (<$vfh>) {
  if ($_ =~ /^\d+ 1:\d+ 2:(\d+) /) {
    $races{$1} .= $_;
  }
}
my %race_prediction_outcomes = (); # race id => 1st, 2nd, 3rd place correct
my @race_prediction_conditionals = ([], [], [], [], []); # predicted_place => (actual places)
my @race_ids = sort (keys %races);
my $rank;
foreach my $race_id (@race_ids) {
  my $vfile = $validation_file;
  $vfile =~ s/format$/race$race_id.format/;
  my $classify_file = $vfile;
  $classify_file =~ s/format$/classify/;

  unless ($just_stats) {
    open(my $vofh, '>', $vfile) or die "Cannot open file $vfile";
    printf $vofh ("%s%s", $vheader, $races{$race_id});
    close($vofh);
    runCmd("svm_classify $vfile $model_file $classify_file");
  }

  ###############################################################
  # Step 4.1: were 1st, 2nd, and 3rd place predictions correct? #
  ###############################################################
  my @rank_actuals = (); # racer positions in the order from the training data.
  print("reading file $classify_file\n");
  open(my $classify_predict_fh, '<', $classify_file);
  while (<$classify_predict_fh>) {
    if ($_ =~ /^([-.0-9e]+)\s*$/) {
      push(@rank_actuals, $1);
    }
  }
  my @prediction_values_on_hyperplane = sort {$a <=> $b} @rank_actuals;

  my $first_place_correct  = $rank_actuals[0] == $prediction_values_on_hyperplane[0];
  my $second_place_correct = $rank_actuals[1] == $prediction_values_on_hyperplane[1];
  my $third_place_correct  = $rank_actuals[2] == $prediction_values_on_hyperplane[2];
  $race_prediction_outcomes{$race_id} = [$first_place_correct,
                                         $second_place_correct,
                                         $third_place_correct];

  for(my $predicted_position = 0; $predicted_position <= $#race_prediction_conditionals; $predicted_position++) {
    push(@{$race_prediction_conditionals[$predicted_position]}, 
      
      hyperplane_val_to_rank(\@rank_actuals, $prediction_values_on_hyperplane[$predicted_position])
    );
  }
}
close($vfh);

#####################################################################################
# Final step: print everything as nicely as possible to copy-paste into spreadsheet #
#####################################################################################
my $first_place_corrects  = 0;
my $second_place_corrects = 0;
my $third_place_corrects  = 0;
foreach my $race_id (@race_ids) {
  my @race_prediction_outcomes = @{$race_prediction_outcomes{$race_id}};
  $first_place_corrects += 1  if $race_prediction_outcomes[0];
  $second_place_corrects += 1 if $race_prediction_outcomes[1];
  $third_place_corrects += 1  if $race_prediction_outcomes[2];
}
my $racelen = $#race_ids + 1.0;
my $first_place_accuracy  = $first_place_corrects  / $racelen;
my $second_place_accuracy = $second_place_corrects / $racelen;
my $third_place_accuracy  = $third_place_corrects  / $racelen;
printf("#accuracy1st\taccuracy2nd\taccuracy3rd\tmeanActual1st\tmeanActual2nd\t...\tmeanActual5th\traceX\tcorrect1st\tcorrect2nd\tcorrect3rd\t...\n");
printf("%f\t%f\t%f",
       $first_place_accuracy,
       $second_place_accuracy,
       $third_place_accuracy);
for (my $predicted_position = 0; $predicted_position <= $#race_prediction_conditionals; $predicted_position++) {
  my @positions = @{$race_prediction_conditionals[$predicted_position]};
  my $sum_actual = 0;
  foreach my $position (@positions) {
    $sum_actual += $position;
  }
  printf("\t%f", $sum_actual / scalar(@positions));
}
foreach my $race_id (@race_ids) {
  my @race_prediction_outcomes = @{$race_prediction_outcomes{$race_id}};
  printf("\trace%d\t%d\t%d\t%d",
         $race_id,
         $race_prediction_outcomes[0],
         $race_prediction_outcomes[1],
         $race_prediction_outcomes[2]);
}
print("\n");

# Done, ask to record data
print <<EOS;
Thanks!
In Google spreadsheet, please now record for svm_learn:
  Regularization parameter C
  Runtime in cpu-seconds
  Number of SV
  L1 loss
  VCdim
  XiAlpha-estimate error
  XiAlpha-estimate recall
  XiAlpha-estimate precision
In Google spreadsheet, please now record for svm_classify:
  1st,2nd,3rd place accuracies and...
  ...for each formula1.$all_years.$outyear.race*.classify
    race id and 1st,2nd,3rd place correct (above)
EOS
