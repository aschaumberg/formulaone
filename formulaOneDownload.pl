#!/usr/bin/env perl

#    formulaOneDownload.pl: downloads formula one information
#    Copyright (C) 2014,2015 Andrew Schaumberg schaumberg.andrew@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use diagnostics;

use Date::Parse; # str2time

my $year_today = (localtime(time))[5] + 1900;
my $OLDEST_YEAR_OF_DATA_TO_FETCH = 2000; # don't fetch data for years earlier than this.  earliest data is from year 1950.
print("#race_year\trace_id\trace_day_seconds_since_epoch\trace_country\tfinishing_position\tcar_number\tdriver_name\tteam\tlaps\ttime_in_milliseconds_measured_or_inferred\tgrid_starting_position\n");
for(my $year = $year_today; $year >= $OLDEST_YEAR_OF_DATA_TO_FETCH; $year--) {
  my $url_base = "http://www.formula1.com/results/season";
  open(my $fh, "wget -O- $url_base/$year/ |") or die $!;
  while (<$fh>) {
    if ($_ =~ /<td><a href="\/results\/season\/$year\/(\d+)\/">([^<]+?)<\/a><\/td>/) {
      my ($race, $country) = (int($1), $2);
      #print("$1\n");
      sleep(1);
      open(my $country_fh, "wget -O- $url_base/$year/$race/ |") or die $!;
      #Pos No      Driver                Team           Laps Time/Retired Grid Pts
      #1   3  Daniel Ricciardo  Red Bull Racing-Renault 70   1:53:05.058  4    25
      #2   14 Fernando Alonso   Ferrari                 70   +5.2 secs    5    18
      #3   44 Lewis Hamilton    Mercedes                70   +5.8 secs    22   15
#raw html
#        <div class="contentContainer">^M
#            <table class="raceResults" cellpadding="0" cellspacing="0" summary="">^M
#                <tr>^M
#                    <th>Pos</th>^M
#                    <th>No</th>^M
#                    <th>Driver</th>^M
#                    <th>Team</th>                    ^M
#^M
#                    ^M
#                    <th>Laps</th>^M
#                    ^M
#                    ^M
#                    <th>Time/Retired</th>^M
#                    ^M
#                    ^M
#                    <th>Grid</th>^M
#                    <th title="Points">Pts</th>^M
#                    ^M
#                    ^M
#                    ^M
#                </tr>^M
#    ^M
#        <tr>^M
#            <td>1</td>^M
#            <td>3</td>^M
#            <td nowrap="nowrap"><a href="/results/driver/2014/857.html">Daniel Ricciardo</a></td>^M
#            <td nowrap="nowrap"><a href="/results/team/2014/2995.html">Red Bull Racing-Renault</a></td>            ^M
#^M
#            ^M
#            <td>70</td>^M
#            ^M
#^M
#            <td>1:53:05.058</td>^M
#         ^M
#            ^M
#            ^M
#            <td>4</td>^M
#            <td>25</td>^M
#            ^M
#            ^M
#            ^M
#        </tr>^M
      my $mode = 0;
      my ($race_day, $previous_pos);
      my ($pos, $no, $driver, $team, $first_laps, $laps,
          $first_time, $time, $prior_time, $grid,
          $finished_race);
      while (<$country_fh>) {
        if ($mode == 0) {
          if ($_ =~ /(\d{1,2})\s+([A-Za-z]{3})\s+$year<\/span>/) {
#        <span>14 - 16 Mar 2014</span>
            $race_day = str2time(sprintf('%s %s %s', $1, $2, $year));
          } elsif ($_ =~ /<th>Pos<\/th>/) {
            #print("POS<1>\n");
            $mode = 1;
          }
        } elsif ($mode >= 1 and $_ =~ /<tr>/) {
          $previous_pos = $pos;
          $pos = $no = $driver = $team = $laps = $time = $grid = $finished_race = undef;
          #print("POS<2>\n");
          $mode = 2;
        } elsif ($mode == 2) {
          if ($_ =~ /<td>(\d+)<\/td>/) {
            #print("POS<3>\n");
            $pos = int($1);
            $finished_race = 1;
            $mode = 3;
          } elsif ($_ =~ /<td>(?:Ret|DNS|NC|DSQ)<\/td>/) {
            $pos = $previous_pos + 1;
            $finished_race = 0;
            $mode = 3;
          }
        } elsif ($mode == 3) {
          if ($_ =~ /<td>(\d+)<\/td>/) {
            #print("POS<4>\n");
            $no = int($1);
            $mode = 4;
          }
        } elsif ($mode == 4) {
          # some drivers have unicode names, such as Kimi Räikkönen
          if ($_ =~ /<td nowrap="nowrap"><a href="[^"]+driver[^"]+">([^<]+)<\/a><\/td>/) {
            #print("POS<5>\n");
            $driver = $1;
            $mode = 5;
          }
        } elsif ($mode == 5) {
          if ($_ =~ /<td nowrap="nowrap"><a href="[^"]+team[^"]+">([-_ a-zA-Z]+)<\/a><\/td>/) {
            #print("POS<6>\n");
            $team = $1;
            $mode = 6;
          }
        } elsif ($mode == 6) {
          if ($_ =~ /<td>(\d+)<\/td>/) {
            $laps = int($1);
            $mode = 7;
          }
        } elsif ($mode == 7) {
#            <td>1:53:05.058</td>^M
          if (!defined($first_time)) {
            # time specified in one of two ways:
            #            <td>1:53:05.058</td>^M
            #            <td>1:38'265.33</td>^M <-- error? http://www.formula1.com/results/season/2001/34/  treat as 1:38:26.33
            if ($_ =~ /<td>(\d+:\d+:\d+.\d+)<\/td>/ or
                $_ =~ /<td>(\d+:\d+'\d+.\d+)<\/td>/) {
              #print ("<<$1>>\n");
              my ($hrs, $mins, $secs, $mils) = split(/[.:']/, $1);
              $mins = int($mins);
              $mins = int($mins / 10) if ($mins >= 60);
              $first_time = (int($hrs) *1000*60*60) +
                            (    $mins *1000*60) +
                            (int($secs)*1000) +
                            int($mils);
              $time = $first_time;
              $first_laps = $laps;
              $mode = 8;
            }
          } else {
            if ($finished_race == 1) {
              if (     $_ =~ /<td>\+([.0-9]+)\s*(?:secs)?<\/td>/) {
#            <td>+26.7 secs</td>
                $time = int($1 * 1000) + $first_time;
                $mode = 8;
              } elsif ($_ =~ /<td>\+([0-9]+)[:']([.0-9]+)\s*(?:secs)?<\/td>/) {
#            <td>+1'26.7 secs</td>
                my ($mins, $secs) = ($1, $2);
                $time = (int($mins)*1000*60) + int($secs * 1000) + $first_time;
                $mode = 8;
#XXX elsif     <td>+1'26.7 secs</td> # XXX
              } elsif ($_ =~ /<td>\+([0-9]+)\s+Laps?<\/td>/) { # +1 Lap, +2 Laps, ...
                my $laps_deficit = int($1);
                my $estimated_time = $first_time + (($first_time / $first_laps)*$laps_deficit);
                $time = int(($estimated_time + $prior_time + 1) / 2); # infer approximately how many milliseconds behind the race leader this car is, and what prior racer time was
                $mode = 8;
              }
            } else {
              if ($_ =~ /<td>\s*([^<]*?)\s*<\/td>/) {
                $time = $1; # XXX: is not a number, can no longer estimate lap times with $prior_time
                $mode = 8;
              }
            }
          }
        } elsif ($mode == 8) {
          if ($_ =~ /<td>(\d+)<\/td>/) {
            $grid = int($1);
            $mode = 1;
          }
          if (defined($race_day) and
              defined($pos) and
              defined($no) and
              defined($driver) and
              defined($team) and
              defined($laps) and
              defined($time) and
              defined($grid)) {
            print("$year\t$race\t$race_day\t$country\t$pos\t$no\t$driver\t$team\t$laps\t$time\t$grid\n");
            $prior_time = $time;
          }
        }
      }
      #exit;
    }
  }
  #exit;
  sleep(1);
}


