formulaOneDownloader.pl downloads race data from formula1.com.

formualOneFormat.pl converts formulaOneDownloader output to SVMlight input.

formulaOnePipeline.pl takes downloaded data and invokes formulaOneFormat.pl and
SVMlight as needed to make predictions, given a desired number of years for
training data and a desired year to predict race outcomes.

Prerequisites: perl, wget

Recommended: SVMlight, from http://svmlight.joachims.org/

Examples:

Download data, assuming 20141020t060550 represents today's date and time:

* formulaOneDownloader.pl > download.20141020t060550.out

Train on years 2000, 2001, and 2002 -- to save the prediction results for each race in 2003:

* formulaOnePipeline.pl download.20141020t060550.out 2000,2001,2002 2003

Train on years 2002, 2003, 2004, and 2005 -- to save the prediction results for each race in 2006:

* formulaOnePipeline.pl download.20141020t060550.out 2002,2003,2004,2005 2006
