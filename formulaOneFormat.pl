#!/usr/bin/env perl

#    formulaOneFormat.pl: converts formulaOneDownload.pl to SVMlight formats.
#    Copyright (C) 2014 Andrew Schaumberg schaumberg.andrew@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# takes formulaOneDownload.pl output as input on STDIN
# output is printed in a format that may be taken as SVMlight input

# pipeline: formulaOneDownload.pl -> formulaOneFormat.pl -> SVMlight

# all input must be read before any output may be written

# predicted label: finishing race rank, a.k.a. finishing_position
# feature 1: race year
# feature 2: race sequence id (the oldest race has id 0; latest race has max id)
# next (c) features: one binary feature for each country (c)
# next (d) features: one binary feature for each driver name (d)
# next (t) features: one binary feature for each team name (t)
#
# omitted feature, arbitrary:
# car number
# omitted features, since these aren't known at the beginning of the race:
# laps
# time in milliseconds measured or inferred

use strict;
use warnings;
use diagnostics;

my @race_sequence = ();
my $race_sequence_id_max = 0;
my @country = ();
my $country_id_max = 0;
my @driver = ();
my $driver_id_max = 0;
my @team = ();
my $team_id_max = 0;
sub get_race_sequence ($) {
  my $race_sequence_timestamp = $_[0];
  for(my $i = 0; $i <= $#race_sequence; $i++) {
    if ($race_sequence[$i] == $race_sequence_timestamp) {
      return $i;
    }
  }
  push(@race_sequence, $race_sequence_timestamp);
  return $race_sequence_id_max++;
}
sub get_country_id ($) {
  my $country_name = $_[0];
  for(my $i = 0; $i <= $#country; $i++) {
    if ($country[$i] eq $country_name) {
      return $i;
    }
  }
  push(@country, $country_name);
  return $country_id_max++;
}
sub get_driver_id ($) {
  my $driver_name = $_[0];
  for(my $i = 0; $i <= $#driver; $i++) {
    if ($driver[$i] eq $driver_name) {
      return $i;
    }
  }
  push(@driver, $driver_name);
  return $driver_id_max++;
}
sub get_team_id ($) {
  my $team_name = $_[0];
  for(my $i = 0; $i <= $#team; $i++) {
    if ($team[$i] eq $team_name) {
      return $i;
    }
  }
  push(@team, $team_name);
  return $team_id_max++;
}

my @rows_unexpanded = ();
my $first_year = 99999;
while (<STDIN>) {
  #parse non-comment lines from formulaOneDownload.pl
  ##race_year	race_id	race_day_seconds_since_epoch	race_country	finishing_position	car_number	driver_name	team	laps	time_in_milliseconds_measured_or_inferred	grid_starting_position
  #2014	914	Australia	1	6	Nico Rosberg	Mercedes	57	5578710	3
  if ($_ =~ /^\d+\s+\d/) { # good enough, call this a match
    my ($race_year_str, $race_id_str, $race_timestamp_str, $country_str,
        $finishing_position_str, $car_number_str, $driver_name_str,
        $team_name_str, $laps_str,
        $time_in_milliseconds_measured_or_inferred_str) = split(/\t/, $_);
    my $race_timestamp = int($race_timestamp_str);
    if ($#race_sequence < 0 or $race_sequence[$#race_sequence] != $race_timestamp) {
      push(@race_sequence, $race_timestamp);
    }

    my $race_year = int($race_year_str);
    $first_year = $race_year if ($race_year < $first_year);

    push(@rows_unexpanded, [
      int($finishing_position_str),
      $race_year,
      $race_timestamp,
      int($car_number_str),
      int($laps_str),
      $time_in_milliseconds_measured_or_inferred_str,
      get_country_id($country_str),
      get_driver_id($driver_name_str),
      get_team_id($team_name_str)
    ]);
  }
}
@race_sequence = sort {$a <=> $b} (@race_sequence);

my $feature_index = 3;
printf("#finishing_position 1:race_year_since_%d 2:race_sequence_id",
       $first_year);
for(my $i = 0; $i <= $#country; $i++) {
  printf(" %d:country_%s", $feature_index, $country[$i] =~ s/\s+//gr);
  $feature_index++;
}
for(my $i = 0; $i <= $#driver; $i++) {
  printf(" %d:driver_%s", $feature_index, $driver[$i] =~ s/\s+//gr);
  $feature_index++;
}
for(my $i = 0; $i <= $#team; $i++) {
  printf(" %d:team_%s", $feature_index, $team[$i] =~ s/\s+//gr);
  $feature_index++;
}
print("\n");

foreach my $row_ref (@rows_unexpanded) {
  my ($finishing_position, $race_year, $race_timestamp, $car_number, $laps,
      $time_in_milliseconds_measured_or_inferred,
      $country_id, $driver_id, $team_id) = @$row_ref;
  printf("%d 1:%d", $finishing_position, $race_year - $first_year);

  for(my $i = 0; $i <= $#race_sequence; $i++) {
    if ($race_sequence[$i] == $race_timestamp) {
      printf(" 2:%d", $i);
      last;
    }
  }

  $feature_index = 3;
  for(my $i = 0; $i <= $#country; $i++) {
    printf(" %d:%d", $feature_index, ($i == $country_id)?(1):(0));
    $feature_index++;
  }
  for(my $i = 0; $i <= $#driver; $i++) {
    printf(" %d:%d", $feature_index, ($i == $driver_id)?(1):(0));
    $feature_index++;
  }
  for(my $i = 0; $i <= $#team; $i++) {
    printf(" %d:%d", $feature_index, ($i == $team_id)?(1):(0));
    $feature_index++;
  }
  print("\n");
}
